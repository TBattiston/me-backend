from os import environ

from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_seeder import FlaskSeeder

app = Flask(__name__, static_folder='../frontend', static_url_path="")
app.config['SQLALCHEMY_DATABASE_URI'] = environ.get('DATABASE_URL')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
Migrate(app, db)

seeder = FlaskSeeder()
seeder.init_app(app, db)

from .models import *

@app.route('/api/work_experiences')
def work_experiences():
    work_experiences = WorkExperience.query.all()
    return jsonify([we.serialize() for we in work_experiences])

@app.route('/api/skills')
def skills():
    skills = Skill.query.all()
    return jsonify([s.serialize() for s in skills])

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    return app.send_static_file('index.html')
