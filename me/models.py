import enum

from . import db

class SkillLevel(enum.Enum):
    INTERESTED = 'Interested'
    DIGGING = 'Digging'
    ADVANCED = 'Advanced'
    SENIOR = 'Senior'

work_experience_skills = db.Table('work_experience_skills',
    db.Column('skill_id', db.Integer, db.ForeignKey('skill.id'), primary_key=True),
    db.Column('work_experience_id', db.Integer, db.ForeignKey('work_experience.id'), primary_key=True),
)

class Skill(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode, nullable=False)
    level = db.Column(db.Enum(SkillLevel))

    def serialize(self):
        return {
            'name': self.name,
            'level': self.level.value
        }

class WorkExperience(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    position = db.Column(db.Unicode, nullable=False)
    company = db.Column(db.Unicode, nullable=False)
    description = db.Column(db.Unicode, nullable=False)
    start_date = db.Column(db.Date, nullable=False)
    end_date = db.Column(db.Date, nullable=True)

    skills = db.relationship('Skill', secondary=work_experience_skills, lazy='subquery', backref=db.backref('work_experiences', lazy=True))

    def serialize(self):
        return {
            'position': self.position,
            'description': self.description,
            'company': self.company,
            'start_date': self.start_date,
            'end_date': self.end_date,
            'skills': [s.serialize() for s in self.skills]
        }
