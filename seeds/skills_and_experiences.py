from datetime import date
from flask_seeder import Seeder, Faker, generator
from me import db
from me.models import *

class SkillsAndExperiencesSeeder(Seeder):
    def run(self):
        skills = {
            'ruby': Skill(name='Ruby', level=SkillLevel.SENIOR),
            'rails': Skill(name='Ruby on Rails', level=SkillLevel.SENIOR),
            'rspec': Skill(name='RSpec', level=SkillLevel.SENIOR),
            'tdd': Skill(name='TDD', level=SkillLevel.SENIOR),
            'psql': Skill(name='PostgreSQL', level=SkillLevel.ADVANCED),
            'js': Skill(name='JS', level=SkillLevel.ADVANCED),
            'python': Skill(name='Python', level=SkillLevel.DIGGING),
            'nodejs': Skill(name='NodeJS', level=SkillLevel.DIGGING),
            'elixir': Skill(name='Elixir', level=SkillLevel.DIGGING),
            'rust': Skill(name='Rust', level=SkillLevel.INTERESTED),
            'rabbitmq': Skill(name='RabbitMQ', level=SkillLevel.DIGGING),
            'php': Skill(name='PHP', level=SkillLevel.DIGGING),
        }

        swile_skills = [
            skills['ruby'],
            skills['rails'],
            skills['rspec'],
            skills['tdd'],
            skills['psql'],
            skills['nodejs'],
            skills['js'],
        ]

        ubi_skills = [
            skills['tdd'],
            skills['psql'],
            skills['js'],
            skills['rabbitmq'],
            skills['php']
        ]

        pl_skills = [
            skills['ruby'],
            skills['rails'],
            skills['rspec'],
            skills['tdd'],
            skills['psql'],
            skills['nodejs'],
            skills['js'],
        ]

        permigo_skills = [
            skills['ruby'],
            skills['rails'],
            skills['rspec'],
            skills['tdd'],
            skills['psql'],
            skills['js'],
        ]

        work_experiences = {
            'swile': WorkExperience(skills=swile_skills, position='Backend Software Engineer', company='Swile (ex Lunchr)', description="Part of a distributed team\nWorking in a micro-services environment\nFacing scaling issues\nTaking ownership on new features", start_date=date(2020, 1, 23), end_date=None),
            'ubi': WorkExperience(skills=ubi_skills, position='Web Application Developer', company='Ubitransport', description="Analyzed migration from a monolith to a microservice architecture\nImplemented new features to the monolith during the migration\nWas part of a team of 5", start_date=date(2019, 10, 1), end_date=date(2019, 12, 31)),
            'pl': WorkExperience(skills=pl_skills, position='CTO', company='PépiteLab', description="Worked mostly remotely Modeled MVPs\nPlanned releases\nWas part of the ideation process\nLed a team of 4\n\nBootstrapped 3 startups, including LesBonsTech which is still up and running", start_date=date(2017, 6, 1), end_date=date(2019, 9, 30)),
            'permigo': WorkExperience(skills=permigo_skills, position='Lead Developer', company='PermiGo', description="Led a team of 6 using SCRUM\nApplied principles of eXtreme Programming\nFollowed clean code principles with a focus on testing\nMaintained existing applications\nDesigned and delivered new applications\nDeployed to production on a daily basis\nCoordinated with other services\nBuilt f rom scratch an online highway code learning platform as a separated project", start_date=date(2014, 10, 6), end_date=date(2017, 5, 31)),
        }

        for skill in list(skills.values()):
            db.session.add(skill)

        for experience in list(work_experiences.values()):
            db.session.add(experience)
