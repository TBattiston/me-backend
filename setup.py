import io

from setuptools import find_packages
from setuptools import setup

setup(
    name="me",
    version="0.1.0",
    maintainer="Thomas Battiston",
    maintainer_email="thomas@battiston.fr",
    description="A basic curriculum vitæ.",
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        "flask",
        "Flask-Migrate",
        "flask-sqlalchemy",
        "psycopg2",
        "Flask-Seeder"
    ],
    extras_require={"test": ["pytest", "coverage", "python-dotenv"]},
)
